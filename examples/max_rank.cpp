#include <mpi.h>
#include "utils/bind.hpp"

int main(){
    bxx::shared_ptr<int> max_rank = 0;

    std::for_each(bxx::nodes::begin(), bxx::nodes::end(), [&](int i){
        bxx::node(i).cpu([](bxx::shared_ptr<int>& max){
            *max = std::max(*max, bxx::rank());
        }, max_rank);
    });

    bxx::sync();
    std::cout << max_rank << std::endl;

    return 0;
}
