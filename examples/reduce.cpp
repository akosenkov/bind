#include "utils/bind.hpp"

int main(){
    std::vector<bxx::shared_ptr<int> > a = {1, 2, 3, 4, 6, 7, 8, 9};

    for(int s = 1; s < a.size(); s *= 2)
    for(int i = s; i < a.size(); i += s*2){

        bxx::node(i % bxx::nodes::size()).cpu([](bxx::shared_ptr<int>& dst, const bxx::shared_ptr<int>& src){
            *dst += *src;
        }, a[i-s], a[i]);

    }

    bxx::sync();
    std::cout << "Reduced value: " << a[0] << "\n";
    return 0;
}
